package com.example.dumpster_app.utils;

import unibo.btlib.BluetoothChannel;

public class C {

    public static final String TOKEN_REQUEST_PATH = "tokenRequestApp.php";
    public static final String ADD_TIME_PATH = "addTime.php";
    public static final String CLOSE_CONNECTION_PATH = "closeConnection.php";
    public static final String APP_LOG_TAG = "BT_CLN";
    public static String serverName = null;
    public static BluetoothChannel btChannel = null;

    public class bluetooth {
        public static final int ENABLE_BT_REQUEST = 1;
        public static final String BT_DEVICE_ACTING_AS_SERVER_NAME = "DSD TECH HC-05";
    }

    public  class authentication {
        public static final String TOKEN_PREFIX = "token:";
        public static final String SERVER_PREFIX = "server:";
        public static final String AUTHENTICATION_OK = "auth:ok";
        public static final String TOKEN_REQUEST = "r_token:1";
        public static final String MSG_DIVIDER = ":";
        public static final String TOKEN_SUBMIT = "s_token";
        public static final String AUTHENTICATION_PREFIX = "auth";
    }

    public class trashtype {
        public static final String SELECT_TRASH_TYPE = "s_t_type";
        public static final String TRASH_SELECTION_OK = "s_t_type:ok";
        public static final String TRASH_SELECTION_ERROR = "s_t_type:error";
    }

    public class timer{
        public static final String ADD_TIMER = "add_t:";
        public static final String TIMER_OK = "add_t:ok";
    }
}
