package com.example.dumpster_app;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.dumpster_app.utils.C;
import com.example.dumpster_app.utils.netutils.Http;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.Timer;
import java.util.TimerTask;

public class TimerActivity extends AppCompatActivity {

    private static final int TIME_INC = 5;
    private static final int INITIAL_TIME = 30;
    private int time;
    private TextView textView;
    private Timer timer;
    private boolean flag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timer);
        time = INITIAL_TIME;
        textView = (TextView)(findViewById(R.id.timeText));
        textView.setText(INITIAL_TIME + "");
        flag = true;
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if(time > 0){
                    Handler mainHandler = new Handler(getMainLooper());
                    Runnable myRunnable = () -> textView.setText(--time + "");
                    mainHandler.post(myRunnable);
                }
                if(time <= 0){
                    tryHttpGet(C.serverName + C.CLOSE_CONNECTION_PATH);
                    C.btChannel.close();
                    flag = false;
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(i);
                }
            }
        }, 0, 1000);
        initUI();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(!flag){
            timer.cancel();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        timer.cancel();
        Http.get((C.serverName + C.CLOSE_CONNECTION_PATH), response -> {});
        C.btChannel.close();
    }

    private void initUI() {
        findViewById(R.id.addTimeButton).setOnClickListener(v -> {
            tryHttpGet(C.serverName + C.ADD_TIME_PATH + "?time=" + TIME_INC);
        });
    }

    private void tryHttpGet(String url) {
        Http.get(url, response -> {
            if (response.code() == HttpURLConnection.HTTP_OK) {
                try {
                    String res = response.contentAsString();
                    if (res.startsWith(C.timer.TIMER_OK)) {
                        C.btChannel.sendMessage(C.timer.ADD_TIMER + TIME_INC*1000);
                        time += TIME_INC;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

}
