package com.example.dumpster_app;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import unibo.btlib.RealBluetoothChannel;

import com.example.dumpster_app.utils.C;
import com.example.dumpster_app.utils.netutils.Http;

import java.util.Timer;
import java.util.TimerTask;

public class TrashSelectionActivity extends AppCompatActivity {
    private static final int TIMEOUT = 10;
    private static final int SERVER_TIME = 30;

    private Timer timer;
    private int time;
    private boolean flag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trash_selection);
        C.btChannel.registerListener(new RealBluetoothChannel.Listener() {
            @Override
            public void onMessageReceived(String receivedMessage) {
                if (receivedMessage.startsWith(C.trashtype.TRASH_SELECTION_OK)) {
                    C.btChannel.removeListener(this);
                    Http.get((C.serverName + C.ADD_TIME_PATH + "?time=" + SERVER_TIME), response -> { });
                    Intent i = new Intent(getApplicationContext(), TimerActivity.class);
                    startActivity(i);
                }
                if (receivedMessage.startsWith(C.trashtype.TRASH_SELECTION_ERROR)) {
                    C.btChannel.removeListener(this);
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(i);
                }
            }
            @Override
            public void onMessageSent(String sentMessage) {
            }
        });
        flag = true;
        time = TIMEOUT;
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                time--;
                if(time <= 0){
                    Http.get((C.serverName + C.CLOSE_CONNECTION_PATH), response -> {});
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    C.btChannel.close();
                    flag = false;
                    startActivity(i);
                }
            }
        }, 0, 1000);
        initUI();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onStop() {
        super.onStop();
        if(!flag) {
            timer.cancel();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        timer.cancel();
        C.btChannel.close();
        Http.get((C.serverName + C.CLOSE_CONNECTION_PATH), response -> {});
    }

    private void initUI() {
        findViewById(R.id.trashA).setOnClickListener(v -> {
            C.btChannel.sendMessage(C.trashtype.SELECT_TRASH_TYPE + C.authentication.MSG_DIVIDER + "a");
            flag = false;
        });
        findViewById(R.id.trashB).setOnClickListener(v -> {
            C.btChannel.sendMessage(C.trashtype.SELECT_TRASH_TYPE + C.authentication.MSG_DIVIDER + "b");
            flag = false;
        });
        findViewById(R.id.trashC).setOnClickListener(v -> {
            C.btChannel.sendMessage(C.trashtype.SELECT_TRASH_TYPE + C.authentication.MSG_DIVIDER + "c");
            flag = false;
        });
    }
}
