package com.example.dumpster_app;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.UUID;

import com.example.dumpster_app.utils.C;
import com.example.dumpster_app.utils.netutils.Http;

import unibo.btlib.BluetoothChannel;
import unibo.btlib.ConnectionTask;
import unibo.btlib.RealBluetoothChannel;
import unibo.btlib.ConnectToBluetoothServerTask;
import unibo.btlib.BluetoothUtils;
import unibo.btlib.exceptions.BluetoothDeviceNotFound;

public class MainActivity extends AppCompatActivity {

    private static final long BUTTON_TIME = 3000;
    private static final int SERVER_TIME = 10;
    private String token = null;
    private String server = null;
    private int attempts = 0;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();

        if (btAdapter != null && !btAdapter.isEnabled()) {
            startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), C.bluetooth.ENABLE_BT_REQUEST);
        }

        initUI();
    }

    @Override
    protected void onResume() {
        super.onResume();
        attempts = 0;
        findViewById(R.id.attemptsCount).setVisibility(View.INVISIBLE);
        findViewById(R.id.requestBtn).setEnabled(false);
        findViewById(R.id.connectBtn).setEnabled(true);
        ((TextView) findViewById(R.id.statusLabel)).setText(String.format("Status : not connected"));
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Http.get((C.serverName + C.CLOSE_CONNECTION_PATH), response -> {});
        C.btChannel.close();
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, @Nullable final Intent data) {
        if (requestCode == C.bluetooth.ENABLE_BT_REQUEST && resultCode == RESULT_OK) {
            Log.d(C.APP_LOG_TAG, "Bluetooth enabled!");
        }

        if (requestCode == C.bluetooth.ENABLE_BT_REQUEST && resultCode == RESULT_CANCELED) {
            Log.d(C.APP_LOG_TAG, "Bluetooth not enabled!");
        }
    }

    private void initUI() {
        ((TextView) findViewById(R.id.attemptsCount)).setVisibility(View.INVISIBLE);
        findViewById(R.id.connectBtn).setOnClickListener(v -> {
            try {
                connectToBTServer();
            } catch (BluetoothDeviceNotFound bluetoothDeviceNotFound) {
                bluetoothDeviceNotFound.printStackTrace();
            }
        });
        findViewById(R.id.requestBtn).setOnClickListener(v -> {
            findViewById(R.id.requestBtn).setEnabled(false);
            final Handler handler = new Handler(getMainLooper());
            handler.postDelayed(() -> findViewById(R.id.requestBtn).setEnabled(true), BUTTON_TIME);
            connectionRequest();
        });
    }

    private void connectToBTServer() throws BluetoothDeviceNotFound {
        final BluetoothDevice serverDevice = BluetoothUtils.getPairedDeviceByName(C.bluetooth.BT_DEVICE_ACTING_AS_SERVER_NAME);

        final UUID uuid = BluetoothUtils.getEmbeddedDeviceDefaultUuid();

        new ConnectToBluetoothServerTask(serverDevice, uuid, new ConnectionTask.EventListener() {
            @Override
            public void onConnectionActive(final BluetoothChannel channel) {

                ((TextView) findViewById(R.id.statusLabel)).setText(String.format("Status : connected to server on device %s",
                        serverDevice.getName()));

                findViewById(R.id.connectBtn).setEnabled(false);
                findViewById(R.id.requestBtn).setEnabled(true);

                C.btChannel = channel;
                C.btChannel.registerListener(new RealBluetoothChannel.Listener() {
                    @Override
                    public void onMessageReceived(String receivedMessage) {
                        if (receivedMessage.startsWith(C.authentication.TOKEN_PREFIX)) {
                            token = "token=" + getMsgContent(receivedMessage);
                            if(server != null){
                                tryHttpGet(server + "?" + token);
                            }
                        }

                        if (receivedMessage.startsWith(C.authentication.SERVER_PREFIX)) {
                            C.serverName = getMsgContent(receivedMessage);
                            server = C.serverName + C.TOKEN_REQUEST_PATH;
                            if(token != null){
                                tryHttpGet(server + "?" + token);
                            }
                        }

                        if (receivedMessage.startsWith(C.authentication.AUTHENTICATION_OK)) {
                            C.btChannel.removeListener(this);
                            tryHttpGet(C.serverName + C.ADD_TIME_PATH + "?time=" + SERVER_TIME);
                            Intent i = new Intent(getApplicationContext(), TrashSelectionActivity.class);
                            startActivity(i);
                        }

                    }

                    @Override
                    public void onMessageSent(String sentMessage) {
                    }
                });
            }

            @Override
            public void onConnectionCanceled() {
                ((TextView) findViewById(R.id.statusLabel)).setText(String.format("Status : unable to connect, device %s not found!",
                        C.bluetooth.BT_DEVICE_ACTING_AS_SERVER_NAME));
            }
        }).execute();
    }

    private  void connectionRequest(){
        attempts++;
        C.btChannel.sendMessage(C.authentication.TOKEN_REQUEST);
        ((TextView) findViewById(R.id.attemptsCount)).setVisibility(View.VISIBLE);
        ((TextView) findViewById(R.id.attemptsCount)).setText(String.format("Request count: %d",
                attempts));
    }

    private String getMsgContent(String msg) {
        int separatorP = msg.indexOf(C.authentication.MSG_DIVIDER);
        return msg.substring(separatorP + 1);
    }

    private void tryHttpGet(String url) {
        Http.get(url, response -> {
            if (response.code() == HttpURLConnection.HTTP_OK) {
                try {
                    String res = response.contentAsString();
                    if (res.startsWith(C.authentication.TOKEN_PREFIX)) {
                        C.btChannel.sendMessage(C.authentication.TOKEN_SUBMIT + C.authentication.MSG_DIVIDER + getMsgContent(res));
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
